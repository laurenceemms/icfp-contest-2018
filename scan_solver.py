# Scan solver
import logging
from nanobots import all_voxels_grounded
from nanobots import HARMONICS_LOW
from nanobots import HARMONICS_HIGH

def solve(model, nano_bots):
    resolution = len(model)
    print('resolution: ' + str(resolution))

    bindex = 0

    # move up one layer
    nano_bots.SMove(nano_bots.GetBID(bindex),
                    0,
                    1,
                    0)
    nano_bots.Commit()

    # scan and print
    count_up_x = True
    count_up_z = True
    for y in range(1, resolution):
        for z in range(resolution):
            for x in range(resolution):
                cx = x
                cy = y
                cz = z
                if not count_up_x:
                    cx = resolution - x - 1
                if not count_up_z:
                    cz = resolution - z - 1
                print('scan xyz: ({}, {}, {})'.format(cx, cy, cz))
                current_cell = nano_bots.GetCurrentCell(bindex)
                dx = cx - current_cell[0]
                dy = cy - current_cell[1]
                dz = cz - current_cell[2]
                if dx == 0 and dy == 0 and dz == 0:
                    continue
                print('delta: {}'.format((dx, dy, dz)))
                # move
                nano_bots.SMove(nano_bots.GetBID(bindex),
                                dx,
                                dy,
                                dz)
                nano_bots.Commit()
                # fill
                if model[cx][cy - 1][cz]:
                    print('fill {} -> {}'.format((cx, cy, cz), (cx, cy-1, cz)))
                    voxels_grounded = all_voxels_grounded(nano_bots, cx, cy-1, cz)
                    if voxels_grounded:
                        if nano_bots.GetHarmonics() == HARMONICS_HIGH:
                            nano_bots.Fill(nano_bots.GetBID(bindex),
                                           0, -1, 0)
                            nano_bots.Commit()
                            nano_bots.Flip(nano_bots.GetBID(bindex))
                            nano_bots.Commit()
                        else:
                            nano_bots.Fill(nano_bots.GetBID(bindex),
                                           0, -1, 0)
                            nano_bots.Commit()
                    else:
                        if nano_bots.GetHarmonics() == HARMONICS_LOW:
                            nano_bots.Flip(nano_bots.GetBID(bindex))
                            nano_bots.Commit()
                            nano_bots.Fill(nano_bots.GetBID(bindex),
                                           0, -1, 0)
                            nano_bots.Commit()
                        else:
                            nano_bots.Fill(nano_bots.GetBID(bindex),
                                           0, -1, 0)
                            nano_bots.Commit()
                px = cx
                py = cy
                pz = cz
            count_up_x = not count_up_x
        count_up_z = not count_up_z

    # return to origin in a straight line - this may fail
    for z in range(resolution - 1):
        cz = cz - 1
        print('return z xyz: ({}, {}, {})'.format(cx, cy, cz))
        current_cell = nano_bots.GetCurrentCell(bindex)
        dx = cx - current_cell[0]
        dy = cy - current_cell[1]
        dz = cz - current_cell[2]
        if dx == 0 and dy == 0 and dz == 0:
            continue
        print('delta: {}'.format((dx, dy, dz)))
        nano_bots.SMove(nano_bots.GetBID(bindex),
                        dx,
                        dy,
                        dz)
        nano_bots.Commit()

    # return to origin in a straight line - this may fail
    for y in range(resolution - 1):
        cy = cy - 1
        print('return y xyz: ({}, {}, {})'.format(cx, cy, cz))
        current_cell = nano_bots.GetCurrentCell(bindex)
        dx = cx - current_cell[0]
        dy = cy - current_cell[1]
        dz = cz - current_cell[2]
        if dx == 0 and dy == 0 and dz == 0:
            continue
        print('delta: {}'.format((dx, dy, dz)))
        nano_bots.SMove(nano_bots.GetBID(bindex),
                        dx,
                        dy,
                        dz)
        nano_bots.Commit()

    nano_bots.Halt(nano_bots.GetBID(bindex))
    nano_bots.Commit()

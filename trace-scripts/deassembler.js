const fs = require('fs')
const argv = require('minimist')(process.argv.slice(2))

if (!argv.trace)
  throw new Error('trace file path required')

const tracePath = argv.trace

// all the crap exec-trace needs
require('browser-env')()
global.traceBData = null
global.bdataLength = (data) => data.length
global.bdataSub = (data, i) => data[i]
global.deepCallback = (result) => {
  console.log(result, '\n')
}
const fakeBody = fs.readFileSync('./fake-body.html', 'utf-8')
document.body.innerHTML = fakeBody

var stdout = document.getElementById('stdout')
hookInnerHTML(stdout, global.deepCallback)


fs.readFile(tracePath, (err, traceData) => {
  if (err) throw err
  global.traceBData = new Uint8Array(traceData)
  require('./chk-trace.js')
  var chkBtn = document.getElementById('chkTrace')
  chkBtn.onclick()
})


/* HELPERS */

function hookInnerHTML(element, callback) {
  var setter = Object.getOwnPropertyDescriptor(Element.prototype, 'innerHTML').set
  Object.defineProperty(Element.prototype, 'innerHTML', {
    set: function(val) {
      if (this === element) callback(val)
      return setter.call(this, val)  // call original setter
    }
  })
}

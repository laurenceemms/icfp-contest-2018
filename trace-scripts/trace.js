const fs = require('fs')
const argv = require('minimist')(process.argv.slice(2))
const path = require('path')

if (!argv.trace)
  throw new Error('trace file path required')

const sourcePath = argv.source
const targetPath = argv.target
const tracePath = argv.trace

// all the crap exec-trace needs
require('browser-env')()
global.srcModelBData = null
global.tgtModelBData = null
global.traceBData = null
global.vis = null
global.bdataLength = (data) => data.length
global.bdataSub = (data, i) => data[i]
global.deepCallback = (result) => {
  if (result.includes('Success') || result.includes('Failure'))
    console.log(result, '\n')
}
const fakeBody = fs.readFileSync(path.join(__dirname, 'fake-body.html'), 'utf-8')
document.body.innerHTML = fakeBody

var stdout = document.getElementById('stdout')
hookInnerHTML(stdout, global.deepCallback)

fs.readFile(tracePath, (err, traceData) => {
  if (err) throw err
  // exec script expects data in typed arrays
  global.srcModelBData = loadFileIfExists(sourcePath, 'srcModelEmpty')
  global.tgtModelBData = loadFileIfExists(targetPath, 'tgtModelEmpty')
  global.traceBData = new Uint8Array(traceData)
  require(path.join(__dirname, 'exec-trace.js'))
  var execBtn = document.getElementById('execTrace')
  execBtn.onclick()
})


/* HELPERS */

function loadFileIfExists(path, checkboxId) {
  if (path)
    return new Uint8Array(fs.readFileSync(path))
  document.getElementById(checkboxId).checked = true
  return null
}

function hookInnerHTML(element, callback) {
  var setter = Object.getOwnPropertyDescriptor(Element.prototype, 'innerHTML').set
  Object.defineProperty(Element.prototype, 'innerHTML', {
    set: function(val) {
      if (this === element) callback(val)
      return setter.call(this, val)  // call original setter
    }
  })
}

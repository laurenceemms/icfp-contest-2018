# Slice reassembler
import logging
from nanobots import all_voxels_grounded
from nanobots import HARMONICS_LOW
from nanobots import HARMONICS_HIGH
from nanobots import make_slices
from nanobots import print_slices

def solve(source_model, target_model, nano_bots):
    resolution = len(model)
    print('resolution: ' + str(resolution))

    bindex = 0

    disassemble_slices(nano_bots, bindex, source_model, make_slices(source_model))
    assemble_slices(nano_bots, bindex, target_model, make_slices(target_model))

    nano_bots.Halt(nano_bots.GetBID(bindex))
    nano_bots.Commit()

import argparse
import errno
import logging
import os
from collections import OrderedDict
from copy import deepcopy
from functools import partial
from multiprocessing import Pool

from model_loader import problem_read

EMPTY_CELL = -1
FULL_CELL = -2
VOLATILE_CELL = -3  # i.e. this cell is used for a command

HARMONICS_LOW = False
HARMONICS_HIGH = True

LOG_ENERGY = False

# commmand list IDs
COMMIT_COMMAND = 'COMMIT'
HALT_COMMAND = 'HALT'
WAIT_COMMAND = 'WAIT'
FLIP_COMMAND = 'FLIP'
SMOVE_COMMAND = 'SMOVE'
LMOVE_COMMAND = 'LMOVE'
FISSION_COMMAND = 'FISSION'
FILL_COMMAND = 'FILL'
VOID_COMMAND = 'VOID'
FUSION_COMMAND = 'FUSION'
GFILL_COMMAND = 'GFILL'
GVOID_COMMAND = 'GVOID'

logging.basicConfig(level=logging.INFO)


class InvalidBidException(Exception):
    pass


class InvalidDestException(Exception):
    pass


class InvalidHarmonicsException(Exception):
    pass


class InvalidLinearCoordinate(Exception):
    pass


class DuplicateCommandException(Exception):
    pass


class InvalidHaltException(Exception):
    pass


class InvalidMoveException(Exception):
    pass


class InvalidFusionException(Exception):
    pass


class InvalidFissionException(Exception):
    pass


class InvalidFillException(Exception):
    pass


class InvalidSLDException(Exception):
    pass


class InvalidLLDException(Exception):
    pass


class InvalidNDException(Exception):
    pass


class InvalidFDException(Exception):
    pass


class InvalidVolatileException(Exception):
    pass


class InvalidEmptyException(Exception):
    pass


class InvalidFullException(Exception):
    pass


def mlen(x, y, z):
    return abs(x) + abs(y) + abs(z)


def clen(x, y, z):
    return max(max(abs(x), abs(y)), abs(z))


def validate_sld(x, y, z):
    return mlen(x, y, z) <= 5


def validate_lld(x, y, z):
    return mlen(x, y, z) <= 15


def validate_nd(x, y, z):
    ml = mlen(x, y, z)
    return ml > 0 and ml <= 2 and clen(x, y, z) == 1


def validate_fd(x, y, z):
    cl = clen(x, y, z)
    return cl > 0 and cl <= 30


class NanoBot():
    def __init__(self, x=0, y=0, z=0, bid=0, seeds=[]):
        self._current_cell = (x, y, z)
        self._bid = bid
        self._seeds = seeds


class NanoBots():
    def __init__(self, resolution, trace_path, initial_state=None):
        self._traces = []
        self._trace_path = trace_path
        self._bots = OrderedDict()
        self._bots[0] = NanoBot(seeds=range(1, 40))
        self._next_bots = OrderedDict()
        self._next_bots[0] = NanoBot(seeds=range(1, 40))
        self._resolution = resolution
        self._command_list = {}
        self._current_world = three_d_cube(EMPTY_CELL, resolution, initial_state)
        self._current_world[0][0][0] = 0
        self._next_world = three_d_cube(EMPTY_CELL, resolution, initial_state)
        self._next_world[0][0][0] = 0
        self._energy = 0
        self._harmonics = HARMONICS_LOW
        # auto built command list
        self._auto_command_list = []

    def __enter__(self):
        return self

    def __exit__(self, etype, value, tb):
        self._traces.append('')
        with open(self._trace_path, 'w') as trace_file:
            trace_file.write('\n'.join(self._traces))
        logging.info("generating nanobots trace %s", self._trace_path)

    def Reset(self):
        self._traces = []
        self._bots = OrderedDict()
        self._bots[0] = NanoBot(seeds=range(1, 40))
        self._next_bots = OrderedDict()
        self._next_bots[0] = NanoBot(seeds=range(1, 40))
        self._command_list = {}
        self._current_world = three_d_cube(EMPTY_CELL, self._resolution)
        self._current_world[0][0][0] = 0
        self._next_world = three_d_cube(EMPTY_CELL, self._resolution)
        self._next_world[0][0][0] = 0
        self._energy = 0
        self._harmonics = HARMONICS_LOW
        # auto built command list
        self._auto_command_list = []

    def GetCommandList(self):
        return self._auto_command_list

    def GetEnergy(self):
        return self._energy

    def GetHarmonics(self):
        return self._harmonics

    def GetBID(self, bid):
        if bid not in self._bots:
            raise InvalidBidException('GetBID Invalid BID: {}'.format(bid))
        if self._bots[bid]._bid != bid:
            raise InvalidBidException(
                'GetBID Invalid BID assigned previously: {} != {}'.format(
                    bid, self._bots[bid]._bid))
        return self._bots[bid]._bid

    def GetCurrentCell(self, bid):
        if bid not in self._bots:
            raise InvalidBidException('GetCurrentCell Invalid BID: {}'.format(bid))
        return self._bots[bid]._current_cell

    def GetSeeds(self, bid):
        if bid not in self._bots:
            raise InvalidBidException('GetSeeds Invalid BID: {}'.format(bid))
        return self._bots[bid]._seeds

    def ClearVolatile(self):
        for (k, z) in enumerate(self._next_world):
            for (j, y) in enumerate(z):
                for (i, x) in enumerate(y):
                    if x == VOLATILE_CELL:
                        self._next_world[k][j][i] = EMPTY_CELL

    def Commit(self):
        # update energy
        res_cubed = self._resolution * self._resolution * self._resolution
        if self._harmonics == HARMONICS_HIGH:
            energy_increment = 30 * res_cubed
            if LOG_ENERGY:
                logging.info('Harmonics are high, adding {}'.format(energy_increment))
            self._energy = self._energy + energy_increment
        elif self._harmonics == HARMONICS_LOW:
            energy_increment = 3 * res_cubed
            if LOG_ENERGY:
                logging.info('Harmonics are low, adding {}'.format(energy_increment))
            self._energy = self._energy + energy_increment

        energy_increment = 20 * len(self._bots)
        if LOG_ENERGY:
            logging.info('Commit: {} bots, adding {}'.format(len(self._bots), energy_increment))
        self._energy = self._energy + energy_increment

        # handle halt and missing commands
        for bid, bot in self._bots.items():
            if bid in self._command_list:
                self._traces.append(self._command_list[bid])
                if self._command_list[bid] == 'Halt':
                    logging.info('Halt called')
                    return True
            else:
                logging.warning("Adding a wait for bot {}. Wasting energy.".format(bid))
                self._traces.append('Wait')

        # clear command list
        self._command_list = {}

        # create the new state of the world
        self.ClearVolatile()
        # copy worlds
        self._current_world = deepcopy(self._next_world)
        # copy bots
        self._bots = deepcopy(self._next_bots)

        # add auto command
        self._auto_command_list.append((COMMIT_COMMAND, []))

        return False

    def out_of_bounds(self, x, y, z):
        return x < 0 or x >= self._resolution or \
            y < 0 or y >= self._resolution or \
            z < 0 or z >= self._resolution

    def is_voxel_empty(self, cx, cy, cz, exception=False):
        """
        Raises an exeption if the voxel is not empty and exception == True
        """
        cell_value = self._next_world[cx][cy][cz]
        if cell_value != EMPTY_CELL:
            if exception:
                raise InvalidEmptyException('Cell {} is not empty value: {}'.format(
                                            (cx, cy, cz), cell_value))
            return False
        return True

    def are_voxels_empty(self, coordinates):
        return all(
            [self.is_voxel_empty(cx, cy, cz) for (cx, cy, cz) in coordinates])

    def is_voxel_full(self, cx, cy, cz, exception=False):
        if self.is_voxel_empty(cx, cy, cz):
            if exception:
                raise InvalidFullException('Cell {} is empty'.format(
                                           (cx, cy, cz)))
            return False
        return True

    def are_voxels_full(self, coordinates):
        return all(
            [self.is_voxel_full(cx, cy, cz) for (cx, cy, cz) in coordinates])

    def set_volatile_cell(self, x, y, z):
        self.is_voxel_empty(x, y, z, exception=True)
        self._next_world[x][y][z] = VOLATILE_CELL

    def Halt(self, bid):
        if bid not in self._bots:
            raise InvalidBidException('Halt BID: ' + str(bid))
        if bid in self._command_list:
            logging.error('Duplicate command for BID: ' + str(bid))
            raise DuplicateCommandException('Halt BID: ' + str(bid))
        if len(self._bots) != 1:
            logging.error('Halt was called with a BID != 0')
            raise InvalidHaltException('Halt BID: ' + str(bid))
        (cx, cy, cz) = self._bots[bid]._current_cell
        if cx != 0 or cy != 0 or cz != 0:
            logging.error('Halt was called when bot was not at the origin: ' +
                          str((cx, cy, cz)))
            raise InvalidHaltException('Halt BID: ' + str(bid))
        if self._harmonics == HARMONICS_HIGH:
            raise InvalidHarmonicsException('Halt BID: ' + str(bid))

        # add command
        self._command_list[bid] = 'Halt'

        # add auto command
        self._auto_command_list.append((HALT_COMMAND, [bid]))

    def Wait(self, bid):
        if bid not in self._bots:
            raise InvalidBidException('Wait BID: ' + str(bid))
        if bid in self._command_list:
            logging.error('Duplicate command for BID: ' + str(bid))
            raise DuplicateCommandException('Wait BID: ' + str(bid))

        # add command
        self._command_list[bid] = 'Wait'

        # add auto command
        self._auto_command_list.append((WAIT_COMMAND, [bid]))

    def Flip(self, bid):
        if bid not in self._bots:
            raise InvalidBidException('Flip BID: ' + str(bid))
        if bid in self._command_list:
            logging.error('Duplicate command for BID: ' + str(bid))
            raise DuplicateCommandException('Flip BID: ' + str(bid))

        # add command
        if self._harmonics == HARMONICS_HIGH:
            self._harmonics = HARMONICS_LOW
        elif self._harmonics == HARMONICS_LOW:
            self._harmonics = HARMONICS_HIGH
        self._command_list[bid] = 'Flip'

        # add auto command
        self._auto_command_list.append((FLIP_COMMAND, [bid]))

    def SMove(self, bid, x, y, z):
        if bid not in self._bots:
            raise InvalidBidException('SMove BID: ' + str(bid) + ' sld: ' +
                                      str((x, y, z)))
        if bid in self._command_list:
            logging.error('Duplicate command for BID: ' + str(bid) + ' sld: ' +
                          str((x, y, z)))
            raise DuplicateCommandException('SMove BID: ' + str(bid) +
                                            ' sld: ' + str((x, y, z)))

        # add command
        (cx, cy, cz) = self._bots[bid]._current_cell
        new_x = cx + x
        new_y = cy + y
        new_z = cz + z
        if sum([c != 0 for c in [x, y, z]]) > 1:
            raise InvalidLinearCoordinate('Exactly one coordinate should be non zero')
        if self.out_of_bounds(new_x, new_y, new_z):
            raise InvalidDestException('SMove BID: ' + str(bid) + ' cxyz: ' + str((cx, cy, cz)) + ' sld: ' +
                                       str((x, y, z)))
        if not validate_lld(x, y, z):
            raise InvalidLLDException('SMove BID: ' + str(bid) + ' sld: ' +
                                      str((x, y, z)))

        if x == 0 and y == 0 and z == 0:
            raise InvalidMoveException('SMove BID: ' + str(bid) + ' cxyz: ' + str((cx, cy, cz)) + ' sld: ' +
                                       str((x, y, z)))

        # validate moving cells
        dx = 0
        dy = 0
        dz = 0
        if x < 0:
            dx = -1
        elif x > 0:
            dx = 1
        if y < 0:
            dy = -1
        elif y > 0:
            dy = 1
        if z < 0:
            dz = -1
        elif z > 0:
            dz = 1
        tx = cx
        ty = cy
        tz = cz
        if tx != new_x:
            while tx != new_x:
                dest = (tx, ty, tz)
                cell_value = self._next_world[tx][ty][tz]
                print(
                    "SMove: current {} txyz {} tvalue {} ".format(
                        (cx, cy, cz), (tx, ty, tz), cell_value))
                if self.out_of_bounds(tx, ty, tz):
                    raise InvalidDestException('SMove Step BID: ' + str(bid) + ' sld: ' +
                                               str((x, y, z)))
                if tx != cx:
                    self.is_voxel_empty(*dest, exception=True)
                tx += dx
        elif ty != new_y:
            while ty != new_y:
                dest = (tx, ty, tz)
                cell_value = self._next_world[tx][ty][tz]
                print(
                    "SMove: current {} txyz {} tvalue {} ".format(
                        (cx, cy, cz), (tx, ty, tz), cell_value))
                if self.out_of_bounds(tx, ty, tz):
                    raise InvalidDestException('SMove Step BID: ' + str(bid) + ' sld: ' +
                                               str((x, y, z)))
                if ty != cy:
                    self.is_voxel_empty(*dest, exception=True)
                ty += dy
        elif tz != new_z:
            while tz != new_z:
                dest = (tx, ty, tz)
                cell_value = self._next_world[tx][ty][tz]
                print(
                    "SMove: current {} txyz {} tvalue {} ".format(
                        (cx, cy, cz), (tx, ty, tz), cell_value))
                if self.out_of_bounds(tx, ty, tz):
                    raise InvalidDestException('SMove Step BID: ' + str(bid) + ' sld: ' +
                                               str((x, y, z)))
                if tz != cz:
                    self.is_voxel_empty(*dest, exception=True)
                tz += dz

        # step through all cells between source and target and make them volatile
        tx = cx
        ty = cy
        tz = cz
        if tx != new_x:
            while tx != new_x:
                self._next_world[tx][ty][tz] = VOLATILE_CELL
                tx += dx
        elif ty != new_y:
            while ty != new_y:
                self._next_world[tx][ty][tz] = VOLATILE_CELL
                ty += dy
        elif tz != new_z:
            while tz != new_z:
                self._next_world[tx][ty][tz] = VOLATILE_CELL
                tz += dz

        # move bot
        self._next_world[cx][cy][cy] = VOLATILE_CELL
        self._next_world[new_x][new_y][new_z] = bid
        self._next_bots[bid]._current_cell = (new_x, new_y, new_z)

        # update energy
        energy_increment = 2 * mlen(x, y, z)
        if LOG_ENERGY:
            logging.info('SMove({}) {}, adding {}'.format(bid, (x, y, z), energy_increment))
        self._energy = self._energy + energy_increment

        self._command_list[bid] = 'SMove <{}, {}, {}>'.format(x, y, z)

        # add auto command
        self._auto_command_list.append((SMOVE_COMMAND, [bid, x, y, z]))

    def LMove(self, bid, x0, y0, z0, x1, y1, z1):
        if bid not in self._bots:
            raise InvalidBidException('LMove BID: ' + str(bid) + ' lld0: ' +
                                      str((x0, y0, z0)) + ' lld1: ' +
                                      str((x1, y1, z1)))
        if bid in self._command_list:
            logging.error('Duplicate command for BID: ' + str(bid))
            raise DuplicateCommandException('LMove BID: ' + str(bid) +
                                            ' lld0: ' + str((x0, y0, z0)) +
                                            ' lld1: ' + str((x1, y1, z1)))
        (cx, cy, cz) = self._bots[bid]._current_cell
        cx1 = cx + x0
        cy1 = cy + y0
        cz1 = cz + z0
        if self.out_of_bounds(cx1, cy1, cz1):
            raise InvalidDestException('LMove BID: ' + str(bid) + ' lld0: ' +
                                       str((x0, y0, z0)) + ' lld1: ' +
                                       str((x1, y1, z1)))

        if (x0 == 0 and y0 == 0 and z0 == 0) or (x1 == 0 and y1 == 0 and z1 == 0):
            raise InvalidMoveException('LMove BID: ' + str(bid) + ' lld0: ' +
                                       str((x0, y0, z0)) + ' lld1: ' +
                                       str((x1, y1, z1)))

        # validate moving cells
        dx0 = 0
        dy0 = 0
        dz0 = 0
        if x0 < 0:
            dx0 = -1
        elif x0 > 0:
            dx0 = 1
        if y0 < 0:
            dy0 = -1
        elif y0 > 0:
            dy0 = 1
        if z0 < 0:
            dz0 = -1
        elif z0 > 0:
            dz0 = 1
        tx = cx
        ty = cy
        tz = cz
        if tx != cx1:
            while tx != cx1:
                dest = (tx, ty, tz)
                if self.out_of_bounds(tx, ty, tz):
                    raise InvalidDestException('LMove Step BID: ' + str(bid) + ' lld0: ' +
                                               str((x0, y0, z0)) + ' lld1: ' +
                                               str((x1, y1, z1)))
                if tx != cx:
                    self.is_voxel_empty(*dest, exception=True)
                tx += dx0
        elif ty != cy1:
            while ty != cy1:
                dest = (tx, ty, tz)
                if self.out_of_bounds(tx, ty, tz):
                    raise InvalidDestException('LMove Step BID: ' + str(bid) + ' lld0: ' +
                                               str((x0, y0, z0)) + ' lld1: ' +
                                               str((x1, y1, z1)))
                if ty != cy:
                    self.is_voxel_empty(*dest, exception=True)
                ty += dy0
        elif tz != cz1:
            while tz != cz1:
                dest = (tx, ty, tz)
                if self.out_of_bounds(tx, ty, tz):
                    raise InvalidDestException('LMove Step BID: ' + str(bid) + ' lld0: ' +
                                               str((x0, y0, z0)) + ' lld1: ' +
                                               str((x1, y1, z1)))
                if tz != cz:
                    self.is_voxel_empty(*dest, exception=True)
                tz += dz0

        cx2 = cx1 + x1
        cy2 = cy1 + y1
        cz2 = cz1 + z1
        if self.out_of_bounds(cx2, cy2, cz2):
            raise InvalidDestException('LMove BID: ' + str(bid) + ' lld0: ' +
                                       str((x0, y0, z0)) + ' lld1: ' +
                                       str((x1, y1, z1)))

        if not validate_sld(x0, y0, z0):
            raise InvalidLLDException('LMove BID: ' + str(bid) + ' lld0: ' +
                                      str((x0, y0, z0)) + ' lld1: ' +
                                      str((x1, y1, z1)))
        if not validate_sld(x1, y1, z1):
            raise InvalidLLDException('LMove BID: ' + str(bid) + ' lld0: ' +
                                      str((x0, y0, z0)) + ' lld1: ' +
                                      str((x1, y1, z1)))

        # validate moving cells
        dx1 = 0
        dy1 = 0
        dz1 = 0
        if x1 < 0:
            dx1 = -1
        elif x1 > 0:
            dx1 = 1
        if y1 < 0:
            dy1 = -1
        elif y1 > 0:
            dy1 = 1
        if z1 < 0:
            dz1 = -1
        elif z1 > 0:
            dz1 = 1
        tx = cx1
        ty = cy1
        tz = cz1
        if tx != cx2:
            while tx != cx2:
                dest = (tx, ty, tz)
                if self.out_of_bounds(tx, ty, tz):
                    raise InvalidDestException('LMove Step BID: ' + str(bid) + ' lld0: ' +
                                               str((x0, y0, z0)) + ' lld1: ' +
                                               str((x1, y1, z1)))
                if tx != cx1:
                    self.is_voxel_empty(*dest, exception=True)
                tx += dx1
        elif ty != cy2:
            while ty != cy2:
                dest = (tx, ty, tz)
                if self.out_of_bounds(tx, ty, tz):
                    raise InvalidDestException('LMove Step BID: ' + str(bid) + ' lld0: ' +
                                               str((x0, y0, z0)) + ' lld1: ' +
                                               str((x1, y1, z1)))
                if ty != cy1:
                    self.is_voxel_empty(*dest, exception=True)
                ty += dy1
        elif tz != cz2:
            while tz != cz2:
                dest = (tx, ty, tz)
                if self.out_of_bounds(tx, ty, tz):
                    raise InvalidDestException('LMove Step BID: ' + str(bid) + ' lld0: ' +
                                               str((x0, y0, z0)) + ' lld1: ' +
                                               str((x1, y1, z1)))
                if tz != cz1:
                    self.is_voxel_empty(*dest, exception=True)
                tz += dz1

        # step through all cells between source and target and make them volatile
        tx = cx
        ty = cy
        tz = cz
        if tx != cx1:
            while tx != cx1:
                self._next_world[tx][ty][tz] = VOLATILE_CELL
                tx += dx0
        elif ty != cy1:
            while ty != cy1:
                self._next_world[tx][ty][tz] = VOLATILE_CELL
                ty += dy0
        elif tz != cz1:
            while tz != cz1:
                self._next_world[tx][ty][tz] = VOLATILE_CELL
                tz += dz0

        self._next_world[cx][cy][cy] = VOLATILE_CELL
        self._next_world[cx1][cy1][cz1] = VOLATILE_CELL

        tx = cx1
        ty = cy1
        tz = cz1
        if tx != cx2:
            while tx != cx2:
                self._next_world[tx][ty][tz] = VOLATILE_CELL
                tx += dx1
        elif ty != cy2:
            while ty != cy2:
                self._next_world[tx][ty][tz] = VOLATILE_CELL
                ty += dy1
        elif tz != cz2:
            while tz != cz2:
                self._next_world[tx][ty][tz] = VOLATILE_CELL
                tz += dz1

        # move bot
        self._next_world[cx2][cy2][cz2] = bid
        self._next_bots[bid]._current_cell = (cx2, cy2, cz2)

        # update energy
        energy_increment = 2 * (mlen(x0, y0, z0) + 2 + mlen(x1, y1, z1))
        if LOG_ENERGY:
            logging.info('LMove({}) {} {}, adding {}'.format(
                bid, (x0, y0, z0), (x1, y1, z1), energy_increment))
        self._energy = self._energy + energy_increment

        self._command_list[bid] = 'LMove <{}, {}, {}> <{}, {}, {}>'.format(
            x0, y0, z0, x1, y1, z1)

        # add auto command
        self._auto_command_list.append((LMOVE_COMMAND, [bid, x0, y0, z0, x1, y1, z1]))

    def Fusion(self, bid0, bid1):
        if bid0 not in self._bots or bid1 not in self._bots:
            raise InvalidBidException('Fusion BID0: ' + str(bid0) + ' BID1: ' +
                                      str(bid1))
        if bid0 in self._command_list:
            logging.error('Duplicate command for BID0: ' + str(bid1))
            raise DuplicateCommandException('Fusion BID0: ' + str(bid0) +
                                            'BID1: ' + str(bid1))
        if bid1 in self._command_list:
            logging.error('Duplicate command for BID1: ' + str(bid1))
            raise DuplicateCommandException('Fusion BID0: ' + str(bid0) +
                                            'BID1: ' + str(bid1))
        (cx0, cy0, cz0) = self._bots[bid0]._current_cell
        (cx1, cy1, cz1) = self._bots[bid1]._current_cell
        dx0 = cx1 - cx0
        dy0 = cy1 - cy0
        dz0 = cz1 - cz0
        dx1 = cx0 - cx1
        dy1 = cy0 - cy1
        dz1 = cz0 - cz1

        if not validate_nd(dx0, dy0, dz0):
            raise InvalidNDException('Fusion BID0: ' + str(bid0) + 'BID1: ' +
                                     str(bid1))

        if not validate_nd(dx1, dy1, dz1):
            raise InvalidNDException('Fusion BID0: ' + str(bid0) + 'BID1: ' +
                                     str(bid1))

        # erase secondary bot
        self._next_bots[bid0]._seeds.extend(self._bots[bid1]._seeds)
        self._next_bots[bid0]._seeds.append(bid1)
        self._next_bots[bid0]._seeds.sort()
        self._next_bots.remove(bid1)
        self._next_world[cx1][cy1][cz1] = EMPTY_CELL

        # update energy
        energy_increment = -24
        if LOG_ENERGY:
            logging.info('Fusion({}) adding {}'.format(bid1, energy_increment))
        self._energy = self._energy + energy_increment

        self._command_list[bid0] = 'FusionP <{}, {}, {}>'.format(dx0, dy0, dz0)
        self._command_list[bid1] = 'FusionS <{}, {}, {}>'.format(dx1, dy1, dz1)

        # add auto command
        self._auto_command_list.append((FUSION_COMMAND, [bid0, bid1]))

    def Fission(self, bid, x, y, z, m):
        if bid not in self._bots:
            raise InvalidBidException('Fission BID: ' + str(bid) + ' nd: ' +
                                      str((x, y, z)) + ' m: ' + str(m))
        if bid in self._command_list:
            logging.error('Duplicate command for BID: ' + str(bid))
            raise DuplicateCommandException('Fission BID: ' + str(bid) +
                                            ' nd: ' + str((x, y, z)) + ' m: ' +
                                            str(m))
        if m <= bid:
            raise InvalidFissionException('Fission BID: ' + str(bid) +
                                          ' nd: ' + str((x, y,
                                                         z)) + ' m: ' + str(m))
        if m not in self._bots[bid]._seeds:
            logging.error('Fusion m is not in bot seeds')
            raise InvalidFissionException('Fission BID: ' + str(bid) +
                                          ' nd: ' + str((x, y,
                                                         z)) + ' m: ' + str(m))

        if not validate_nd(x, y, z):
            raise InvalidNDException('Fission BID: ' + str(bid) + ' nd: ' +
                                     str((x, y, z)) + ' m: ' + str(m))

        (cx, cy, cz) = self._bots[bid]._current_cell
        dest = (cx + x, cy + y, cz + z)
        if self.out_of_bounds(dest[0], dest[1], dest[2]):
            raise InvalidDestException('Fission BID: ' + str(bid) + ' nd: ' +
                                       str((x, y, z)) + ' m: ' + str(m))
        self.is_voxel_empty(*dest, exception=True)

        # split bots
        split_seeds_index = self._next_bots[bid]._seeds.find(m)
        split_seeds_before = self._next_bots[bid]._seeds
        self._next_bots[bid]._seeds = split_seeds_before[:split_seeds_index]
        self._next_bots[m] = NanoBot(x=dest[0], y=dest[1], z=dest[2],
                                     bid=m, seeds=split_seeds_before[split_seeds_index + 1:])
        self._next_world[dest[0]][dest[1]][dest[2]] = m

        # update energy
        energy_increment = 24
        if LOG_ENERGY:
            logging.info('Fission({}) adding {}'.format(bid, energy_increment))
        self._energy = self._energy + energy_increment

        self._command_list[bid] = 'Fission <{}, {}, {}> {}'.format(x, y, z, m)

        # add auto command
        self._auto_command_list.append((FISSION_COMMAND, [bid, x, y, z, m]))

    def Fill(self, bid, x, y, z):
        if bid not in self._bots:
            raise InvalidBidException('Fill BID: ' + str(bid) + ' nd: ' +
                                      str((x, y, z)))
        if bid in self._command_list:
            logging.error('Duplicate command for BID: ' + str(bid))
            raise DuplicateCommandException('Fill BID: ' + str(bid) + ' nd: ' +
                                            str((x, y, z)))
        (cx, cy, cz) = self._bots[bid]._current_cell

        if not validate_nd(x, y, z):
            raise InvalidNDException('Fill BID: ' + str(bid) + ' nd: ' +
                                     str((x, y, z)))

        # We don't fill full cells - Laurence
        dest = (cx + x, cy + y, cz + z)
        if self.out_of_bounds(dest[0], dest[1], dest[2]):
            raise InvalidDestException('Fill BID: ' + str(bid) + ' nd: ' +
                                       str((x, y, z)))
        self.is_voxel_empty(*dest, exception=True)

        # fill the cell
        self._next_world[dest[0]][dest[1]][dest[2]] = FULL_CELL

        # update energy
        energy_increment = 12
        if LOG_ENERGY:
            logging.info('Fill({}) adding {}'.format(bid, energy_increment))
        self._energy = self._energy + 12

        self._command_list[bid] = 'Fill <{}, {}, {}>'.format(x, y, z)

        # add auto command
        self._auto_command_list.append((FILL_COMMAND, [bid, x, y, z]))

    def Void(self, bid, x, y, z):
        if bid not in self._bots:
            raise InvalidBidException('Void BID: ' + str(bid) + ' nd: ' +
                                      str((x, y, z)))
        if bid in self._command_list:
            logging.error('Duplicate command for BID: ' + str(bid))
            raise DuplicateCommandException('Void BID: ' + str(bid) + ' nd: ' +
                                            str((x, y, z)))
        if not validate_nd(x, y, z):
            raise InvalidNDException('Void BID: ' + str(bid) + ' nd: ' +
                                     str((x, y, z)))

        # We don't void empty cells - Laurence
        (cx, cy, cz) = self._bots[bid]._current_cell
        dest = (cx + x, cy + y, cz + z)
        if self.out_of_bounds(dest[0], dest[1], dest[2]):
            raise InvalidDestException('Void BID: ' + str(bid) + ' nd: ' +
                                       str((x, y, z)))
        self.is_voxel_full(*dest, exception=True)

        # empty the cell
        self._next_world[dest[0]][dest[1]][dest[2]] = EMPTY_CELL

        # update energy
        energy_increment = -12
        if LOG_ENERGY:
            logging.info('Void({}) adding {}'.format(bid, energy_increment))
        self._energy = self._energy + energy_increment

        self._command_list[bid] = 'Void <{}, {}, {}>'.format(x, y, z)

        # add auto command
        self._auto_command_list.append((VOID_COMMAND, [bid, x, y, z]))

    def GFill(self, bid, nx, ny, nz, fx, fy, fz):
        if bid not in self._bots:
            raise InvalidBidException('GFill BID: ' + str(bid) + ' nd: ' + str(
                (nx, ny, nz)) + ' fd: ' + str((fx, fy, fz)))
        if bid in self._command_list:
            logging.error('Duplicate command for BID: ' + str(bid))
            raise DuplicateCommandException('GFill BID: ' + str(bid) +
                                            ' nd: ' + str((nx, ny, nz)) +
                                            ' fd: ' + str((fx, fy, fz)))

        if not validate_nd(nx, ny, nz):
            raise InvalidNDException('GFill BID: ' + str(bid) + ' nd: ' + str(
                (nx, ny, nz)) + ' fd: ' + str((fx, fy, fz)))
        if not validate_fd(fx, fy, fz):
            raise InvalidFDException('GFill BID: ' + str(bid) + ' nd: ' + str(
                (nx, ny, nz)) + ' fd: ' + str((fx, fy, fz)))

        (cx, cy, cz) = self._bots[bid]._current_cell
        # TODO: test gfill

        # TODO: fill region

        # TODO: update energy

        self._command_list[bid] = 'GFill <{}, {}, {}> <{}, {}, {}>'.format(
            nx, ny, nz, fx, fy, fz)

        # add auto command
        self._auto_command_list.append((GFILL_COMMAND, [bid, nx, ny, nz, fx, fy, fz]))

    def GVoid(self, bid, nx, ny, nz, fx, fy, fz):
        if bid not in self._bots:
            raise InvalidBidException('GVoid BID: ' + str(bid) + ' nd: ' + str(
                (nx, ny, nz)) + ' fd: ' + str((fx, fy, fz)))
        if bid in self._command_list:
            logging.error('Duplicate command for BID: ' + str(bid))
            raise DuplicateCommandException('GVoid BID: ' + str(bid) +
                                            ' nd: ' + str((nx, ny, nz)) +
                                            ' fd: ' + str((fx, fy, fz)))

        if not validate_nd(nx, ny, nz):
            raise InvalidNDException('GVoid BID: ' + str(bid) + ' nd: ' + str(
                (nx, ny, nz)) + ' fd: ' + str((fx, fy, fz)))
        if not validate_fd(fx, fy, fz):
            raise InvalidFDException('GVoid BID: ' + str(bid) + ' nd: ' + str(
                (nx, ny, nz)) + ' fd: ' + str((fx, fy, fz)))

        (cx, cy, cz) = self._bots[bid]._current_cell
        # TODO: test gvoid

        # TODO: void region

        # TODO: update energy

        self._command_list[bid] = 'GVoid <{}, {}, {}> <{}, {}, {}>'.format(
            nx, ny, nz, fx, fy, fz)

        # add auto command
        self._auto_command_list.append((GVOID_COMMAND, [bid, nx, ny, nz, fx, fy, fz]))


def resolve_model(output_directory, solver_module_name, model, problem_type):
    model_name, model_property = model
    solver_module = __import__(solver_module_name)
    solve = getattr(solver_module, 'solve')
    if problem_type == 'A':
        suffix = '_tgt.mdl'
    elif problem_type == 'D':
        suffix = '_src.mdl'
    else:
        raise Exception('Unhandled problem_type')
    trace_name = model_name.replace(suffix, '.nbta')
    trace_path = os.path.join(output_directory, trace_name)
    if problem_type == 'A':
        with NanoBots(model_property['resolution'], trace_path) as nanobots:
            solve(model_property['target'], nanobots)
    elif problem_type == 'D':
        source = model_property['source']
        with NanoBots(model_property['resolution'], trace_path, source) as nanobots:
            solve(model_property['source'], nanobots)


def resolve_models(output_directory,
                   solver_module_name,
                   problem,
                   problem_number,
                   problem_type):
    try:
        os.makedirs(output_directory)
    except EnvironmentError as e:
        if e.errno == errno.EEXIST and os.path.isdir(output_directory):
            pass
        else:
            raise

    model = problem_read(problem, problem_number, problem_type)
    resolve_model(output_directory, solver_module_name, next(iter(model.items())), problem_type)

    # models = problem_read(problem, problem_number, problem_type)
    # f = partial(resolve_model, output_directory, solver_module_name)
    # with Pool() as p:
    #     p.map(f, models.items())


def main(
        problems_txt=os.path.join(
            os.path.dirname(__file__), 'data', 'problemsF', 'problemsF.txt'),
        output_directory=os.path.join(
            os.path.dirname(__file__), 'output', 'nbta'),
        solver='dumb_solver'):
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-problem', default=problems_txt)
    parser.add_argument('-n', '--problem-number', type=int)
    parser.add_argument('-t', '--problem-type', default='A', choices=['A', 'D', 'R'])
    parser.add_argument('-o', '--output-directory', default=output_directory)
    parser.add_argument('-s', '--solver', default=solver)
    args = parser.parse_args()
    resolve_models(args.output_directory, args.solver, args.input_problem,
                   args.problem_number, args.problem_type)


def do_command(nano_bots, command_id, args_list):
    if command_id == COMMIT_COMMAND:
        nano_bots.Commit()
    elif command_id == HALT_COMMAND:
        nano_bots.Halt(args_list[0])  # bid
    elif command_id == WAIT_COMMAND:
        nano_bots.Wait(args_list[0])  # bid
    elif command_id == FLIP_COMMAND:
        nano_bots.Flip(args_list[0])  # bid
    elif command_id == SMOVE_COMMAND:
        nano_bots.SMove(args_list[0],  # bid
                        args_list[1], args_list[2], args_list[3])  # lld
    elif command_id == LMOVE_COMMAND:
        nano_bots.LMove(args_list[0],  # bid
                        args_list[1], args_list[2], args_list[3],  # sld0
                        args_list[4], args_list[5], args_list[6])  # sld1
    elif command_id == FISSION_COMMAND:
        nano_bots.Fusion(args_list[0],  # bid0
                         args_list[1])  # bid1
    elif command_id == FILL_COMMAND:
        nano_bots.Fill(args_list[0],  # bid
                       args_list[1], args_list[2], args_list[3])  # nd
    elif command_id == VOID_COMMAND:
        nano_bots.Void(args_list[0],  # bid
                       args_list[1], args_list[2], args_list[3])  # nd
    elif command_id == FUSION_COMMAND:
        nano_bots.Fusion(args_list[0],  # bid
                         args_list[1], args_list[2], args_list[3],  # nd
                         args_list[4])  # m
    elif command_id == GFILL_COMMAND:
        # TODO: gfill call
        nano_bots.GFill(args_list[0],  # bid
                        args_list[1], args_list[2], args_list[3],  # nd
                        args_list[4], args_list[5], args_list[6])  # fd
    elif command_id == GVOID_COMMAND:
        # TODO: gvoid call
        nano_bots.GFill(args_list[0],  # bid
                        args_list[1], args_list[2], args_list[3],  # nd
                        args_list[4], args_list[5], args_list[6])  # fd


def do_commands(nano_bots, commands):
    for (command_id, args_list) in commands:
        do_command(nano_bots, command_id, args_list)


def move_to_location(nano_bots, bid, x, y, z, y_first = True):
    current_cell = nano_bots.GetCurrentCell(bid)
    cx = current_cell[0]
    cy = current_cell[1]
    cz = current_cell[2]
    dx = x - cx
    dy = y - cy
    dz = z - cz
    if dx < 0:
        dx = -1
    elif dx > 0:
        dx = 1
    if dy < 0:
        dy = -1
    elif dy > 0:
        dy = 1
    if dz < 0:
        dz = -1
    elif dz > 0:
        dz = 1

    print('move t {} c {} d {}'.format((x, y, z), (cx, cy, cz), (dx, dy, dz)))

    if y_first:
        # move in y first
        if cy != y:
            while cy != y:
                cy += dy
                if cy != current_cell[1]:
                    nano_bots.SMove(bid, 0, dy, 0)
                    nano_bots.Commit()

    # move in x second
    if cx != x:
        while cx != x:
            cx += dx
            if cx != current_cell[0]:
                nano_bots.SMove(bid, dx, 0, 0)
                nano_bots.Commit()

    # move in z third
    if cz != z:
        while cz != z:
            cz += dz
            if cz != current_cell[2]:
                nano_bots.SMove(bid, 0, 0, dz)
                nano_bots.Commit()

    if not y_first:
        # move in y last
        if cy != y:
            while cy != y:
                cy += dy
                if cy != current_cell[1]:
                    nano_bots.SMove(bid, 0, dy, 0)
                    nano_bots.Commit()

def make_slices(model):
    resolution = len(model)
    slices = []
    resolution = len(model)
    for y in range(resolution):
        # start slice
        slice_data_found = False
        slice_min_x = 0
        slice_min_z = 0
        slice_max_x = 0
        slice_max_z = 0
        for z in range(resolution):
            for x in range(resolution):
                if model[x][y][z]:
                    if slice_data_found:
                        slice_min_x = min(slice_min_x, x)
                        slice_min_z = min(slice_min_z, z)
                        slice_max_x = max(slice_max_x, x)
                        slice_max_z = max(slice_max_z, z)
                    else:
                        slice_data_found = True
                        slice_min_x = x
                        slice_min_z = z
                        slice_max_x = x
                        slice_max_z = z
        if slice_data_found:
            slices.append(((slice_min_x, y, slice_min_z), (slice_max_x, y, slice_max_z)))
        else:
            return slices
    return slices


def all_voxels_grounded(nano_bots,
                        ax, ay, az): # voxel to be added
    resolution = nano_bots._resolution

    all_sets = []
    y = 0
    for z in range(resolution):
        for x in range(resolution):
            if nano_bots._current_world[x][0][z] == FULL_CELL or (ax == x and ay == 0 and az == z):
                voxel_grounded = 1
                voxel_set = {(x, y, z)}
                # for each neighbor
                for (nx, ny, nz) in [(x+1, y, z), (x-1, y, z), (x, y, z+1), (x, y, z-1)]:
                    sets_to_remove = []
                    # find neighbor in all sets
                    for (g, s) in all_sets:
                        if (nx, ny, nz) in s:
                            voxel_grounded = max(voxel_grounded, g)
                            voxel_set = voxel_set.union(s)
                            if not (g, s) in sets_to_remove:
                                sets_to_remove.append((g, s))

                    for r in sets_to_remove:
                        all_sets.remove(r)

                all_sets.append((voxel_grounded, voxel_set))

    for y in range(1, resolution):
        for z in range(resolution):
            for x in range(resolution):
                if nano_bots._current_world[x][y][z] == FULL_CELL or (ax == x and ay == y and az == z):
                    voxel_grounded = 0
                    voxel_set = {(x, y, z)}
                    # for each neighbor
                    for (nx, ny, nz) in [(x+1, y, z), (x-1, y, z), (x, y, z+1), (x, y, z-1), (x, y-1, z)]:
                        # find neighbor in all sets
                        sets_to_remove = []
                        for (g, s) in all_sets:
                            if (nx, ny, nz) in s:
                                voxel_grounded = max(voxel_grounded, g)
                                voxel_set = voxel_set.union(s)
                                if not (g, s) in sets_to_remove:
                                    sets_to_remove.append((g, s))

                        for r in sets_to_remove:
                            all_sets.remove(r)

                    all_sets.append((voxel_grounded, voxel_set))

    for (g, s) in all_sets:
        if g == 0:
            return False

    return True

def removed_all_voxels_grounded(nano_bots,
                                rx, ry, rz): # voxel to be removed
    resolution = nano_bots._resolution

    all_sets = []
    y = 0
    for z in range(resolution):
        for x in range(resolution):
            if nano_bots._current_world[x][0][z] == FULL_CELL and (not (rx == x and ry == 0 and rz == z)):
                voxel_grounded = 1
                voxel_set = {(x, y, z)}
                # for each neighbor
                for (nx, ny, nz) in [(x+1, y, z), (x-1, y, z), (x, y, z+1), (x, y, z-1)]:
                    sets_to_remove = []
                    # find neighbor in all sets
                    for (g, s) in all_sets:
                        if (nx, ny, nz) in s:
                            voxel_grounded = max(voxel_grounded, g)
                            voxel_set = voxel_set.union(s)
                            if not (g, s) in sets_to_remove:
                                sets_to_remove.append((g, s))

                    for r in sets_to_remove:
                        all_sets.remove(r)

                all_sets.append((voxel_grounded, voxel_set))

    for y in range(1, resolution):
        for z in range(resolution):
            for x in range(resolution):
                if nano_bots._current_world[x][y][z] == FULL_CELL and (not (rx == x and ry == y and rz == z)):
                    voxel_grounded = 0
                    voxel_set = {(x, y, z)}
                    # for each neighbor
                    for (nx, ny, nz) in [(x+1, y, z), (x-1, y, z), (x, y, z+1), (x, y, z-1), (x, y-1, z)]:
                        # find neighbor in all sets
                        sets_to_remove = []
                        for (g, s) in all_sets:
                            if (nx, ny, nz) in s:
                                voxel_grounded = max(voxel_grounded, g)
                                voxel_set = voxel_set.union(s)
                                if not (g, s) in sets_to_remove:
                                    sets_to_remove.append((g, s))

                        for r in sets_to_remove:
                            all_sets.remove(r)

                    all_sets.append((voxel_grounded, voxel_set))

    for (g, s) in all_sets:
        if g == 0:
            return False

    return True

def print_slices(nano_bots, bid, model, slices):
    for (slice_min, slice_max) in slices:
        print('slice: ({}, {})'.format(slice_min, slice_max))
        cy = slice_min[1]   # should be the same as slice_max[1]

        # move above the current slice start location
        print('move to {}'.format((slice_min[0], cy + 1, slice_min[2])))
        move_to_location(nano_bots, bid, slice_min[0], cy + 1, slice_min[2])

        current_cell = nano_bots.GetCurrentCell(bid)
        print('current cell: ' + str(current_cell))

        # start scanning the slice
        count_x_up = True
        for cz in range(slice_min[2], slice_max[2] + 1):
            print('row {}'.format(cz))
            for cx in range(slice_min[0], slice_max[0] + 1):
                # fill coordinates
                fx = cx
                fy = cy
                fz = cz
                if not count_x_up:
                    fx = slice_min[0] + (slice_max[0] - cx)
                print('fill {} ({})'.format((fx, fy, fz), (cx, cy, cz)))

                current_cell = nano_bots.GetCurrentCell(bid)
                print('current cell: ' + str(current_cell))
                if model[fx][fy][fz]:
                    voxels_grounded = all_voxels_grounded(nano_bots, fx, fy, fz)
                    if voxels_grounded:
                        if nano_bots.GetHarmonics() == HARMONICS_HIGH:
                            nano_bots.Fill(bid, 0, -1, 0)
                            nano_bots.Commit()
                            nano_bots.Flip(bid)
                            nano_bots.Commit()
                        else:
                            nano_bots.Fill(bid, 0, -1, 0)
                            nano_bots.Commit()
                    else:
                        if nano_bots.GetHarmonics() == HARMONICS_LOW:
                            nano_bots.Flip(bid)
                            nano_bots.Commit()
                            nano_bots.Fill(bid, 0, -1, 0)
                            nano_bots.Commit()
                        else:
                            nano_bots.Fill(bid, 0, -1, 0)
                            nano_bots.Commit()
                if count_x_up:
                    nano_bots.SMove(bid, 1, 0, 0)
                else:
                    nano_bots.SMove(bid, -1, 0, 0)
                nano_bots.Commit()
            count_x_up = not count_x_up
            nano_bots.SMove(bid, 0, 0, 1)
            nano_bots.Commit()
            if count_x_up:
                nano_bots.SMove(bid, 1, 0, 0)
            else:
                nano_bots.SMove(bid, -1, 0, 0)
            nano_bots.Commit()

    if nano_bots.GetHarmonics() == HARMONICS_HIGH:
        nano_bots.Flip(bid)
        nano_bots.Commit()

    move_to_location(nano_bots, bid, 0, 0, 0, y_first = False)


def disassemble_slices(nano_bots, bid, model, slices):
    for (slice_min, slice_max) in reversed(slices):
        print('slice: ({}, {})'.format(slice_min, slice_max))
        cy = slice_min[1]   # should be the same as slice_max[1]

        # move above the current slice start location
        print('move to {}'.format((slice_min[0], cy + 1, slice_min[2])))
        move_to_location(nano_bots, bid, slice_min[0], cy + 1, slice_min[2])

        current_cell = nano_bots.GetCurrentCell(bid)
        print('current cell: ' + str(current_cell))

        # start scanning the slice
        count_x_up = True
        for cz in range(slice_min[2], slice_max[2] + 1):
            print('row {}'.format(cz))
            for cx in range(slice_min[0], slice_max[0] + 1):
                # fill coordinates
                fx = cx
                fy = cy
                fz = cz
                if not count_x_up:
                    fx = slice_min[0] + (slice_max[0] - cx)
                print('fill {} ({})'.format((fx, fy, fz), (cx, cy, cz)))

                current_cell = nano_bots.GetCurrentCell(bid)
                print('current cell: ' + str(current_cell))
                if model[fx][fy][fz]:
                    voxels_grounded = removed_all_voxels_grounded(nano_bots, fx, fy, fz)
                    if voxels_grounded:
                        if nano_bots.GetHarmonics() == HARMONICS_HIGH:
                            nano_bots.Void(bid, 0, -1, 0)
                            nano_bots.Commit()
                            nano_bots.Flip(bid)
                            nano_bots.Commit()
                        else:
                            nano_bots.Void(bid, 0, -1, 0)
                            nano_bots.Commit()
                    else:
                        if nano_bots.GetHarmonics() == HARMONICS_LOW:
                            nano_bots.Flip(bid)
                            nano_bots.Commit()
                            nano_bots.Void(bid, 0, -1, 0)
                            nano_bots.Commit()
                        else:
                            nano_bots.Void(bid, 0, -1, 0)
                            nano_bots.Commit()
                if count_x_up:
                    nano_bots.SMove(bid, 1, 0, 0)
                else:
                    nano_bots.SMove(bid, -1, 0, 0)
                nano_bots.Commit()
            count_x_up = not count_x_up
            nano_bots.SMove(bid, 0, 0, 1)
            nano_bots.Commit()
            if count_x_up:
                nano_bots.SMove(bid, 1, 0, 0)
            else:
                nano_bots.SMove(bid, -1, 0, 0)
            nano_bots.Commit()

    if nano_bots.GetHarmonics() == HARMONICS_HIGH:
        nano_bots.Flip(bid)
        nano_bots.Commit()

    move_to_location(nano_bots, bid, 0, 0, 0, y_first = False)


def three_d_cube(val, dim, initial_state=None):
    rv = [[[val for _ in range(dim)] for _ in range(dim)] for _ in range(dim)]
    if initial_state:
        for y in range(dim):
            for z in range(dim):
                for x in range(dim):
                    if initial_state[x][y][z]:
                        rv[x][y][z] = FULL_CELL
    return rv


if __name__ == "__main__":
    main()

import argparse
from functools import partial
import logging
from multiprocessing import Pool
import os
import re
import struct

logging.basicConfig(level=logging.INFO)


def bits_by_bits(f):
    bytes = (b for b in f.read())
    for b in bytes:
        for i in range(8):
            yield (b >> i) & 1


def load_model(model_path):
    with open(model_path, 'rb') as model:
        resolution_byte = model.read(1)
        resolution_R = struct.unpack('B', resolution_byte)[0]
        logging.info("resolution_R %s", resolution_R)
        model_array = [[[0 for k in range(resolution_R)] for j in range(resolution_R)] for i in range(resolution_R)]

        cell_values = bits_by_bits(model)
        for x in range(resolution_R):
            for y in range(resolution_R):
                for z in range(resolution_R):
                    model_array[x][y][z] = next(cell_values)
        logging.info("model_path %s loaded", model_path)
        return model_array


def load_model_from_problem(problem_dir, model_line, problem_type):
    if problem_type == 'A':
        pattern = '^(?P<model_name>\w\w\d+_tgt.mdl):: res (?P<res>\d+); (?P<link>.*) \((?P<description>.*)\)$'
    elif problem_type == 'D':
        pattern = '^(?P<model_name>\w\w\d+_src.mdl):: res (?P<res>\d+); (?P<link>.*) \((?P<description>.*)\)$'
    else:
        raise Exception('Unknown problem_type')
    reg = re.search(pattern, model_line)
    model_name = reg.group('model_name')
    res = int(reg.group('res'))
    link = reg.group('link')
    description = reg.group('description')
    logging.info("model_name: %s, res: %s, link: %s, description: %s", model_name, res, link, description)
    model_data = {'resolution': res, 'link': link, 'description': description}
    if problem_type == 'A':
        model_data['target'] = load_model(os.path.join(problem_dir, model_name))
    elif problem_type == 'D':
        model_data['source'] = load_model(os.path.join(problem_dir, model_name))
    return {model_name: model_data}


def problem_read(problems_txt, model_number, problem_type):
    problem_dir = os.path.dirname(problems_txt)
    with open(problems_txt) as problems:
        content = problems.readlines()
    content = [x.strip() for x in content if x[1] == problem_type]
    return load_model_from_problem(problem_dir, content[model_number - 1], problem_type)


def main(problems_txt=os.path.join(os.path.dirname(__file__), 'data', 'problemsF', 'problemsF.txt')):
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input-problem', default=problems_txt)
    parser.add_argument('-n', '--problem-number', type=int)
    args = parser.parse_args()
    problem_read(args.input_problem, args.problem_number)


if __name__ == "__main__":
    main()

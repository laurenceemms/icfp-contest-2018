#pragma once

#include <cstddef>
#include <vector>

#if WIN32
using ssize_t unsigne long long;
#endif

class NanoBotCommand
{
};
class SingleNanoBotCommand : public NanoBotCommand
{
};
class GroupNanoBotCommand : public NanoBotCommand
{
};

struct NanoBot
{
    bool apply(NanoBotCommand);
    // Nanobot state
};

enum class Harmonic
{
    Low,
    High
};

struct NMMS // Nanobot Matter Manipulation System
{
    // System param
    const size_t seed = 39;

    // System state
    size_t time = 0;

    std::vector<NanoBot> nano_bots();
    Harmonic harmonic_state = Harmonic::Low;
};

struct Coordinate
{
    size_t x;
    size_t y;
    size_t z;
};

struct DCoordinate
{
    ssize_t dx;
    ssize_t dy;
    ssize_t dz;
};

struct LLD : public DCoordinate
{
};
struct SLD : public DCoordinate
{
};
struct ND : public DCoordinate
{
};
struct FD : public DCoordinate
{
};

size_t mlen(const DCoordinate dc);
size_t clen(const DCoordinate dc);

class Halt : public SingleNanoBotCommand
{
};
class Wait : public SingleNanoBotCommand
{
};
class Flip : public SingleNanoBotCommand
{
};

class SMove : public SingleNanoBotCommand
{
    SMove(LLD dc);
};
class LMove : public SingleNanoBotCommand
{
    LMove(SLD dc1, SLD dc2);
};
class Fission : public SingleNanoBotCommand
{
    Fission(ND dc, unsigned char id);
};
class Fill : public SingleNanoBotCommand
{
    Fill(ND dc);
};
class Void : public SingleNanoBotCommand
{
    Void(ND dc);
};

class FusionP : public GroupNanoBotCommand
{
    FusionP(ND nd);
};
class FusionS : public GroupNanoBotCommand
{
    FusionS(ND nd);
};
class GFill : public GroupNanoBotCommand
{
    GFill(ND nd, FD fd);
};
class GVoid : public GroupNanoBotCommand
{
    GVoid(ND nd, FD fd);
};

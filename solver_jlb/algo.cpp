#include <all.h>
#include <mdl.h>

bool
run_naive_sim(MDL mdl)
{
    return true;
}

bool
run(MDL mdl)
{
    return run_naive_sim(std::move(mdl));
}

#include <all.h>
#include <mdl.h>

std::istream&
operator>>(std::istream& is, MDL& mdl)
{
    is >> mdl.resolution;
    std::cerr << int(mdl.resolution) << ",";
    return is;
}

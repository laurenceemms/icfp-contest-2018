#pragma once

#include <all.h>

struct MDL
{
    unsigned char resolution;
    boost::multi_array<int, 3> world;
};

std::istream& operator>>(std::istream& is, MDL& mdl);

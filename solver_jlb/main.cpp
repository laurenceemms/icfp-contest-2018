#include <all.h>

#include <algo.h>
#include <mdl.h>

int main(int argc, char **argv) {
  boost::program_options::options_description desc;
  desc.add_options()("help", "help msg")(
      "model", boost::program_options::value<std::string>(),
      "input model file");

  boost::program_options::variables_map vm;
  boost::program_options::store(
      boost::program_options::parse_command_line(argc, argv, desc), vm);
  boost::program_options::notify(vm);

  if (argc == 1 || vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  std::ifstream ifs(vm["model"].as<std::string>());
  MDL mdl;
  ifs >> mdl;
  run(std::move(mdl));
  return 0;
}

#!/usr/bin/env python

import sys
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Nanobot Optimizer')
    parser.add_argument('-i', '--input')
    parser.add_argument('-o', '--output')
    args = parser.parse_args()

    with open(args.input, 'r') as f:
        all_lines = f.readlines()
        all_lines.append('')

    print('Optimizing to file: ' + str(args.output))

    with open(args.output, 'w') as f:
        accumulator = 1

        for l in range(len(all_lines) - 1):
            line = all_lines[l].strip()
            next_line = all_lines[l + 1].strip()
            if line.strip() == '':
                continue

            print('Command: ' + line.strip())
            print('next: ' + next_line.strip())
            if line == next_line:
                # line is the same as the next line, accumulate
                if line == 'SMove <1, 0, 0>':
                    accumulator += 1
                elif line == 'SMove <-1, 0, 0>':
                    accumulator += 1
                elif line == 'SMove <0, 1, 0>':
                    accumulator += 1
                elif line == 'SMove <0, -1, 0>':
                    accumulator += 1
                elif line == 'SMove <0, 0, 1>':
                    accumulator += 1
                elif line == 'SMove <0, 0, -1>':
                    accumulator += 1
                else:
                    f.write(line.strip() + '\n')
                    accumulator = 1
            else:
                # line differs from next line, flush accumulator if necessary
                if accumulator > 1:
                    if line == 'SMove <1, 0, 0>':
                        print('writing accumulated smove <1, 0, 0>')
                        x = 0
                        while x < accumulator:
                            x_prev = x
                            x += 15
                            f.write('SMove <{}, 0, 0>\n'.format(min(x - x_prev, accumulator - x_prev)))
                    elif line == 'SMove <-1, 0, 0>':
                        print('writing accumulated smove <-1, 0, 0>')
                        x = 0
                        while x < accumulator:
                            x_prev = x
                            x += 15
                            f.write('SMove <{}, 0, 0>\n'.format(-min(x - x_prev, accumulator - x_prev)))
                    elif line == 'SMove <0, 1, 0>':
                        print('writing accumulated smove <0, 1, 0>')
                        x = 0
                        while x < accumulator:
                            x_prev = x
                            x += 15
                            f.write('SMove <0, {}, 0>\n'.format(min(x - x_prev, accumulator - x_prev)))
                    elif line == 'SMove <0, -1, 0>':
                        print('writing accumulated smove <0, -1, 0>')
                        x = 0
                        while x < accumulator:
                            x_prev = x
                            x += 15
                            f.write('SMove <0, {}, 0>\n'.format(-min(x - x_prev, accumulator - x_prev)))
                    elif line == 'SMove <0, 0, 1>':
                        print('writing accumulated smove <0, 0, 1>')
                        x = 0
                        while x < accumulator:
                            x_prev = x
                            x += 15
                            f.write('SMove <0, 0, {}>\n'.format(min(x - x_prev, accumulator - x_prev)))
                    elif line == 'SMove <0, 0, -1>':
                        print('writing accumulated smove <0, 0, -1>')
                        x = 0
                        while x < accumulator:
                            x_prev = x
                            x += 15
                            f.write('SMove <0, 0, {}>\n'.format(-min(x - x_prev, accumulator - x_prev)))
                else:
                    f.write(line.strip() + '\n')
                accumulator = 1

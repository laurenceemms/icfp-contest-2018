import argparse
import logging
import math
import struct

import numpy as np

logging.basicConfig(level=logging.INFO)


def load_from_file(f):
    resolution = f.read(1)[0]
    logging.info("Resolution: {}".format(resolution))
    raw_bytes = np.fromfile(
        f, dtype='>B', count=math.ceil((resolution**3) / 8))
    # Read each byte from least to most significant bit
    bits = np.unpackbits(raw_bytes).reshape(-1, 8)[:, ::-1]
    # Remove padding and reshape to a 3D matrix
    model = bits[:resolution**3].reshape(resolution, resolution, resolution)
    return resolution, model


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--input', help='Input model', required=True)
    args = parser.parse_args()

    with open(args.input, 'rb') as f:
        r, m = load_from_file(f)


if __name__ == '__main__':
    main()

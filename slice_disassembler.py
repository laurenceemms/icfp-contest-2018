# Slice disassembler
from nanobots import make_slices
from nanobots import disassemble_slices

def solve(model, nano_bots):
    resolution = len(model)
    print('resolution: ' + str(resolution))

    bindex = 0

    disassemble_slices(nano_bots, bindex, model, make_slices(model))

    nano_bots.Halt(nano_bots.GetBID(bindex))
    nano_bots.Commit()

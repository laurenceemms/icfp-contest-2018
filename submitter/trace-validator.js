const fs = require('fs')
const path = require('path')
const runScript = require('./run-script')

const problems = path.join(__dirname, '../data/problemsF/')
const traces = path.join(__dirname, '../data/dfltTracesF/')
const executor = path.join(__dirname, '../trace-scripts/trace.js')

const modelPaths = fs.readdirSync(problems).filter(fn => fn.endsWith('.mdl'))
const tracePaths = fs.readdirSync(traces).filter(fn => fn.endsWith('.nbt'))

/*
Holds filepaths in this format:

Fxxxx: {
  source: <source_model_path>,  // optional if target exists
  target: <target_model_path>,  // optional if source exists
  trace: <trace_file_path>      // always exists
}
*/
var argsMap = {}

for (let i = 0, problemNum, path, tracePath; i < modelPaths.length; ++i) {
  path = modelPaths[i]
  problemNum = path.split('_')[0]
  tracePath = tracePaths.find(path => path.includes(problemNum))
  if (!tracePath) {
    console.error('Could not find trace file for problem #' + problemNum)
    console.error('Only running for traces found up until now...')
    break
  }
  if (!argsMap.hasOwnProperty(problemNum))
    argsMap[problemNum] = {}
  if (path.includes('_tgt'))
    Object.assign(argsMap[problemNum], {target: path})
  else
    Object.assign(argsMap[problemNum], {source: path})
  Object.assign(argsMap[problemNum], {trace: tracePath})
}

const allProblems = Object.keys(argsMap)
const options = {cwd: path.dirname(executor)}
var i = 0   // adjust me to start at a different problem

execute()

function execute() {
  var problem = argsMap[allProblems[i]]
  if (!problem) return

  args = ['--trace', traces + problem.trace]
  if (problem.source)
    args = args.concat(['--source', problems + problem.source])
  else if (problem.target)
    args = args.concat(['--target', problems + problem.target])

  console.log('Running:', allProblems[i], '\n')
  runScript(executor, args, options, function(err) {
    if (err) throw err
    console.log('#####'.repeat(10), '\n')
    i++
    execute()
  });
}

const exec = require('child_process').exec
const fs = require('fs')
const path = require('path')

const ROOT_DIR = path.join(__dirname, '..')
const inputDir = path.join(__dirname, '../output/nbta/')
const outputDir = path.join(__dirname, '../output/nbt/')

if (!fs.existsSync(outputDir)){
  fs.mkdirSync(outputDir);
}

const inputFiles = fs.readdirSync(inputDir).filter(fn => fn.endsWith('.nbta'))

const options = {cwd: ROOT_DIR, maxBuffer: 1024 * 1024}   // 1 MiB buffer
var i = 0

execute()

function execute() {
  var file = inputFiles[i]
  if (!file) return

  var inPath = path.join(inputDir, file)
  var outPath = path.join(outputDir, file.replace('.nbta', '.nbt'))

  // console.log(file, inPath, outPath)
  var process = exec(
    `cat ${inPath} | python assembler.py ${outPath}`,
    options,
    (err, stdout, stderr) => {
      if (err) throw err
    }
  )

  process.on('exit', code => {
    if (!code) {
      i++
      execute()
    }
  })
}

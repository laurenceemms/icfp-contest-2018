#!/usr/bin/env python

import argparse
import model_loader

if __name__ == '__main__':
    print('Naive solver')

    parser = argparse.ArgumentParser(description='Naive solver')
    parser.add_argument('filename')
    parser.add_argument('line_number')

    args = parser.parse_args()
    print('Reading file: ' + args.filename)
    print('Line number: ' + args.line_number)

    model = model_loader.problem_read(args.filename, int(args.line_number))

    dx = len(model)
    dy = len(model[0])
    dz = len(model[0][0])

    print('Volume dimensions: ' + str(dx) + ', ' + str(dy) + ', ' + str(dz))

    # create empty grid to populate
    grid = [[[0 for i in range(dx)] for j in range(dy)] for k in range(dz)]


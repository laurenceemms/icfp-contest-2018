# Slice solver
import logging
from nanobots import all_voxels_grounded
from nanobots import HARMONICS_LOW
from nanobots import HARMONICS_HIGH
from nanobots import make_slices
from nanobots import print_slices

def solve(model, nano_bots):
    resolution = len(model)
    print('resolution: ' + str(resolution))

    bindex = 0

    print_slices(nano_bots, bindex, model, make_slices(model))

    nano_bots.Halt(nano_bots.GetBID(bindex))
    nano_bots.Commit()

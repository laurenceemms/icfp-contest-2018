# scan list solver
import logging

def solve(model, nano_bots):
    print('len(model): ' + str(len(model)))

    bindex = 0

    # build command list
    command_list = []
    for z, z_array in enumerate(model):
        #if len(model) == z + 1:
            #continue
        #else:
            #command_list.append((SMOVE_COMMAND, [nano_bots.GetBID(bindex), 0, 0, 1]))
            #command_list.append((COMMIT_COMMAND, []))
        for y, y_list in enumerate(z_array):
            enumerate_y = enumerate(y_list)
            if y % 2 == 1:
                enumerate_y = reversed(list(enumerate_y))

            for x, cell in enumerate_y:
                print('xyz: ({}, {}, {})'.format(x, y, z))
                #print('energy: {}'.format(nano_bots.GetEnergy()))
                if cell:
                    #print('fill cell: bid: {} {}'.format(nano_bots.GetBID(bindex), (x, y, z)))
                    command_list.append((FILL_COMMAND, [nano_bots.GetBID(0), 0, 0, -1]))
                    command_list.append((COMMIT_COMMAND, []))

                bcell = nano_bots.GetCurrentCell(bindex)
                #print('nano_bot[0]: {}'.format(nano_bots.GetCurrentCell(bindex)))
                dx = x - nano_bots.GetCurrentCell(bindex)[0]
                dy = y - nano_bots.GetCurrentCell(bindex)[1]
                dz = z - nano_bots.GetCurrentCell(bindex)[2]
                print('delta: {}'.format((dx, dy, dz)))
                nano_bots.SMove(nano_bots.GetBID(bindex),
                                dx,
                                dy,
                                dz)
                nano_bots.Commit()
                print('Done\n')

#!/usr/bin/env python

# Usage: cat test_trace.nbta | ./assembler.py "test_trace.nbt"

import argparse
import fileinput
import logging
import sys
from functools import reduce

# Commands

# All commands are case insensitive.

# halt == [11111111] == 0xff
# wait == [11111101] == 0xfe
# flip == [11111101] == 0xfd
# smove lld == [00<<lld.a>>0100] [000<<lld.i>>]
# lmove sld1 sld2 == [<<sld2.a>><sld1.a>>1100]
# fusionp nd == [<<nd>>111]
# fusions nd == [<<nd>>110]
# fission nd m == [<<nd>>101] [<<m>>]
# fill nd == [<<nd>>011]

# lld, sld, and nd arguments are written as comma-separated vectors in <> brackets.

# If you have questions about this, ask Laurence

def test_string():
    return 'Halt\n' + \
           'Wait\n' + \
           'Flip\n' + \
           'SMove <12, 0, 0>\n' + \
           'LMove <3, 0, 0> <0, -5, 0>\n' + \
           'FusionP <-1, 1, 0>\n' + \
           'fusions <1,-1,0>\n' + \
           'FISSION <0, 0,1> 5\n' + \
           'Fill <0,-1,0>\n' + \
           'void <1,0, 1>\n' + \
           'gfill <0, -1, 0> <10,-15,20>\n' + \
           'gVoid <1,0,0> <5,5,-5>\n' + \
           'FOO <1, 0, 1>\n' + \
           ' \n' + \
           'Fill\n' + \
           'fusionP (10, 2, 3)\n'

x_direction = 1
y_direction = 2
z_direction = 3

def parse_nd(args):
    if args[0] != '<':
        logging.error('Failed to parse_nd <')
        return -1
    if args[-1] != '>':
        logging.error('Failed to parse_nd >')
        return -1

    v = args[1:-1].split(',')
    if len(v) != 3:
        logging.error('Incorrect dimension in parse_nd')
        return -1

    coords = list(map(int, v))
    for c in coords:
        if c < -1 or c > 1:
            logging.error('Invalid nd coordinates: ' + str(coords))
            return -1

    num_zeros = reduce(lambda x, y: x + int(y == 0), coords, 0)
    if num_zeros == 0:
        logging.error('Invalid nd coordinates: ' + str(coords))
        return -1
    
    return (coords[0] + 1) * 9 + (coords[1] + 1) * 3 + coords[2] + 1

def parse_fd(args):
    if args[0] != '<':
        logging.error('Failed to parse_fd <')
        return (-1, -1, -1)
    if args[-1] != '>':
        logging.error('Failed to parse_fd >')
        return (-1, -1, -1)

    v = args[1:-1].split(',')
    if len(v) != 3:
        logging.error('Incorrect dimension in parse_fd')
        return (-1, -1, -1)

    coords = list(map(int, v))
    for c in coords:
        if c < -30 or c > 30:
            logging.error('Invalid fd coordinates: ' + str(coords))
            return (-1, -1, -1)

    num_zeros = reduce(lambda x, y: x + int(y == 0), coords, 0)
    if num_zeros == 3:
        logging.error('Invalid fd coordinates: ' + str(coords))
        return (-1, -1, -1)
    
    return ((coords[0] + 30), (coords[1] + 30), (coords[2] + 30))

def parse_ld(args, offset):
    if args[0] != '<':
        logging.error('Failed to parse_ld <')
        return (0, 0)
    if args[-1] != '>':
        logging.error('Failed to parse_ld >')
        return (0, 0)

    v = args[1:-1].split(',')
    if len(v) != 3:
        logging.error('Incorrect dimension in parse_ld')
        return (0, 0)

    coords = list(map(int, v))
    if coords[0] != 0:
        if coords[1] != 0 or coords[2] != 0:
            logging.error('Invalid ld coordinates: ' + str(coords))
            return (0, 0)
        else:
            if coords[0] < -offset or coords[0] > offset:
                logging.error('Invalid ld coordinates range: ' + str(coords))
                return (0, 0)
            return (x_direction, coords[0] + offset)
    elif coords[1] != 0:
        if coords[0] != 0 or coords[2] != 0:
            logging.error('Invalid ld coordinates: ' + str(coords))
            return (0, 0)
        else:
            if coords[1] < -offset or coords[1] > offset:
                logging.error('Invalid ld coordinates range: ' + str(coords))
                return (0, 0)
            return (y_direction, coords[1] + offset)
    elif coords[2] != 0:
        if coords[0] != 0 or coords[1] != 0:
            logging.error('Invalid ld coordinates: ' + str(coords))
            return (0, 0)
        else:
            if coords[2] < -offset or coords[2] > offset:
                logging.error('Invalid ld coordinates range: ' + str(coords))
                return 0
            return (z_direction, coords[2] + offset)
    else:
        logging.error('Invalid ld coordinates: ' + str(coords))
        return (0, 0)

def parse_sld(args):
    return parse_ld(args, 5)

def parse_lld(args):
    return parse_ld(args, 15)

def smove(args):
    (lld_a, lld_i) = parse_lld(args)
    return ((((lld_a << 4) + 4) << 8) + lld_i, 2)

def lmove(args):
    index = args[1:].find('<')
    if index == -1:
        logging.error('Invalid lmove arguments')
        return (0, 0)
    (sld1_a, sld1_i) = parse_sld(args[:index+1])
    (sld2_a, sld2_i) = parse_sld(args[index+1:])
    return ((((sld2_a << 6) + (sld1_a << 4) + 12) << 8) + (sld2_i << 4) + sld1_i, 2)

def fusionp(args):
    nd = parse_nd(args)
    if nd == -1:
        return (0, 0)
    return ((nd << 3) + 7, 1)

def fusions(args):
    nd = parse_nd(args)
    if nd == -1:
        return (0, 0)
    return ((nd << 3) + 6, 1)

def fission(args):
    index = args.find('>')
    if index == -1:
        logging.error('Invalid fission arguments')
        return (0, 0)
    nd = parse_nd(args[:index+1])
    if nd == -1:
        return (0, 0)
    m = int(args[index+1:])
    if m < 0 or m > 255:
        logging.error('Invalid fission m value: ' + str(m))
        return (0, 0)
    return ((((nd << 3) + 5) << 8) + m, 2)

def fill(args):
    nd = parse_nd(args)
    if nd == -1:
        return (0, 0)
    return ((nd << 3) + 3, 1)

def voidop(args):
    nd = parse_nd(args)
    if nd == -1:
        return (0, 0)
    return ((nd << 3) + 2, 1)

def gfill(args):
    index = args.find('>')
    if index == -1:
        logging.error('Invalid gfill arguments')
        return (0, 0)
    nd = parse_nd(args[:index+1])
    if nd == -1:
        return (0, 0)
    (fd_x, fd_y, fd_z) = parse_fd(args[index+1:])
    if fd_x == -1:
        return (0, 0)
    return ((((nd << 3) + 1) << 24) + (fd_x << 16) + (fd_y << 8) + fd_z, 4)

def gvoid(args):
    index = args.find('>')
    if index == -1:
        logging.error('Invalid gvoid arguments')
        return (0, 0)
    nd = parse_nd(args[:index+1])
    if nd == -1:
        return (0, 0)
    (fd_x, fd_y, fd_z) = parse_fd(args[index+1:])
    if fd_x == -1:
        return (0, 0)
    return (((nd << 3) << 24) + (fd_x << 16) + (fd_y << 8) + fd_z, 4)

def code_gen(command):
    c = command.lower().strip().split(' ')
    if len(c) > 0:
        if c[0] == 'halt':
            return (0xff, 1)
        elif c[0] == 'wait':
            return (0xfe, 1)
        elif c[0] == 'flip':
            return (0xfd, 1)
        elif c[0] == 'smove':
            if len(c) > 1:
                return smove(''.join(c[1:]))
            else:
                logging.error('Invalid smove command')
                return (0, 0)
        elif c[0] == 'lmove':
            if len(c) > 1:
                return lmove(''.join(c[1:]))
            else:
                logging.error('Invalid lmove command')
                return (0, 0)
        elif c[0] == 'fusionp':
            if len(c) > 1:
                return fusionp(''.join(c[1:]))
            else:
                logging.error('Invalid fusionp command')
                return (0, 0)
        elif c[0] == 'fusions':
            if len(c) > 1:
                return fusions(''.join(c[1:]))
            else:
                logging.error('Invalid fusions command')
                return (0, 0)
        elif c[0] == 'fission':
            if len(c) > 1:
                return fission(''.join(c[1:]))
            else:
                logging.error('Invalid fission command')
                return (0, 0)
        elif c[0] == 'fill':
            if len(c) > 1:
                return fill(''.join(c[1:]))
            else:
                logging.error('Invalid fill command')
                return (0, 0)
        elif c[0] == 'void':
            if len(c) > 1:
                return voidop(''.join(c[1:]))
            else:
                logging.error('Invalid void command')
                return (0, 0)
        elif c[0] == 'gfill':
            if len(c) > 1:
                return gfill(''.join(c[1:]))
            else:
                logging.error('Invalid gfill command')
                return (0, 0)
        elif c[0] == 'gvoid':
            if len(c) > 1:
                return gvoid(''.join(c[1:]))
            else:
                logging.error('Invalid gvoid command')
                return (0, 0)
        else:
            logging.error('Invalid command')
            return (0, 0)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Nanobot Assembler')
    parser.add_argument('filename')

    args = parser.parse_args()
    print('Assembling to file: ' + str(args.filename))

    with open(args.filename, 'bw+') as f:
        for line in sys.stdin:
            if line.strip() == '':
                continue
            print('Command: ' + line.strip())
            (code, b) = code_gen(line)
            if b == 0:
                logging.error('Skipping invalid command: ' + line)
                continue
            print(str(code.to_bytes(b, 'big')) + ' ++ ' + str(b))
            print(format(code, '#010b'))
            f.write(code.to_bytes(b, 'big'))
